@extends('admin.layout.template')


@section('title')
    | Product
@endsection

@push('css-plugins')
    <link rel="stylesheet" href="{{ asset('AdminLTE/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('AdminLTE/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
@endpush

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Products</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">Products</li>
                            <li class="breadcrumb-item active">Index</li>
                        </ol>
                    </div>
                </div>
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <a href="{{ route('product.create') }}" class="btn btn-md btn-outline-primary">Create</a>
                    </div>
                </div>
                @if (session('msg'))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                {{ session('msg') }}
                            </div>
                        </div>

                    </div>
                @endif
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Data Index</h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"
                                        title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                    <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                                        <i class="fas fa-times"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <table class="table table-striped" id="table-index">
                                    <thead>
                                        <tr>
                                            <th style="width: 1%">
                                                #
                                            </th>
                                            <th style="width: 20%">
                                                Product Image
                                            </th>
                                            <th style="width: 10%">
                                                Product Name
                                            </th>
                                            <th>
                                                Price
                                            </th>
                                            <th>
                                                Stock
                                            </th>
                                            <th>
                                                Description
                                            </th>
                                            <th style="width: 20%">
                                                Action
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($data as $item)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>
                                                    <img src="{{ $item['image'] }}" alt="" class="img-fluid"
                                                        style="max-width: 250px">
                                                </td>
                                                <td>{{ $item['name'] }}</td>
                                                <td>Rp {{ number_format($item['price'], 0, ',', '.') }}</td>
                                                <td>{{ $item['stock'] }}</td>
                                                <td>{!! $item['description'] !!}</td>
                                                <td>
                                                    <button class="btn btn-sm btn-outline-danger btnDelete"
                                                        data-id="{{ $item['id'] }}">
                                                        Hapus
                                                    </button>
                                                    <a href="{{ route('product.edit', $item['id']) }}"
                                                        class="btn btn-sm btn-outline-info">
                                                        Edit
                                                    </a>

                                                </td>
                                            </tr>
                                        @empty
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- Default box -->

            <!-- /.card -->

        </section>
        <!-- /.content -->
    </div>
@endsection

@push('js-plugins')
    <script src="{{ asset('AdminLTE/plugins/datatables/jquery.dataTables.min.js') }}"></script>

    <script src="{{ asset('AdminLTE/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
@endpush


@push('js')
    <script>
        $(document).ready(function() {
            $('#table-index').DataTable({
                "columnDefs": [{
                    "targets": [4, 5],
                    "orderable": false
                }]
            });


            $(document).on('click', '.btnDelete', function() {
                var id = $(this).data('id');
                if (confirm('Are you sure you want to delete this data ?')) {
                    $.ajax({
                        type: "DELETE",
                        url: "/product/" + id,
                        data: {
                            _token: '{{ csrf_token() }}',
                        },
                        dataType: "JSON",
                        success: function(response) {
                            alert(response);
                            location.reload();
                        }
                    });
                } else {}
            });

        });
    </script>
@endpush
