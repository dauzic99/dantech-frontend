@extends('admin.layout.template')


@section('title')
    | Create Product
@endsection

@push('css-plugins')
@endpush

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Create Product</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('product.index') }}">Products</a> </li>
                            <li class="breadcrumb-item active">Create</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-primary">
                        <form action="{{ route('product.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="card-header">
                                <h3 class="card-title">Product Form</h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"
                                        title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="inputName">Product Name</label>
                                                    <input type="text" id="inputName" class="form-control" name="name"
                                                        value="{{ old('name') }}">
                                                    @error('name')
                                                        <span class=" text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md 12">
                                                <div class="form-group">
                                                    <label for="inputDescription">Product Description</label>
                                                    <textarea id="inputDescription" class="form-control summer" rows="4" name="description">{{ old('description') }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="inputPrice">Product Price</label>
                                                    <input type="number" id="inputPrice" class="form-control"
                                                        name="price" value="{{ old('price') }}" min="0">
                                                    @error('price')
                                                        <span class=" text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="inputStock">Product Stock</label>
                                                    <input type="number" id="inputStock" class="form-control"
                                                        name="stock" value="{{ old('stock') }}" min="0">
                                                    @error('stock')
                                                        <span class=" text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group mb-4">
                                                    <label class="form-label text-md-left">Product Image</label>
                                                    <div class="">
                                                        <input type="file" name="image" placeholder="Choose image"
                                                            id="image">
                                                    </div>
                                                    @error('image')
                                                        <span class=" text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-12 text-center">
                                                <img id="preview-image" src="{{ asset('image/product-1.jpg') }}"
                                                    alt="preview image" style="max-height: 250px;" class="img-fluid">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row m-3">
                                <div class="col-12">
                                    <input type="submit" value="Save" class="btn btn-success float-right">
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </form>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
@endsection

@push('js-plugins')
@endpush


@push('js')
    <script>
        $(document).ready(function() {
            $('.summer').summernote({
                placeholder: 'Deskripsi Produk Anda Disini',
                tabsize: 5,
                height: 300
            });

            imagePreview('#image', '#preview-image');
        });

        function imagePreview(inputSelector, imageSelector) {
            $(inputSelector).change(function() {
                let reader = new FileReader();
                reader.onload = (e) => {
                    $(imageSelector).attr('src', e.target.result);
                }
                reader.readAsDataURL(this.files[0]);
            });
        }
    </script>
@endpush
