<!DOCTYPE html>
<html lang="en">

<head>
    @include('admin.layout.head')
</head>

<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">

        @include('admin.layout.preloader')

        <!-- Navbar -->
        @include('admin.layout.navbar')
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        @include('admin.layout.sidebar')

        <!-- Content Wrapper. Contains page content -->
        @yield('content')
        <!-- /.content-wrapper -->

        @include('admin.layout.footer')

        <!-- Control Sidebar -->
        @include('admin.layout.control')
        <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->

    @include('admin.layout.script')
</body>

</html>
