<!DOCTYPE html>
<html lang="en">

<head>
    @include('admin.layout.auth.head')
</head>

<body class="hold-transition login-page">
    @yield('content')

    @include('admin.layout.auth.script')
</body>

</html>
