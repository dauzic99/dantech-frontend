<?php

use Illuminate\Support\Facades\Http;

if (!function_exists('ApiPost')) {
    function ApiPost($url, $data = [])
    {
        return Http::acceptJson()->post(env('API_URL') . $url, $data);
    }
}
if (!function_exists('ApiPostCredential')) {
    function ApiPostCredential($url, $body = [])
    {
        $response = Http::withToken(session('token'))->post(env('API_URL') . $url, $body);
        return $response;
    }
}

if (!function_exists('ApiPostCredentialWithImage')) {
    function ApiPostCredentialWithImage($url, $body = [])
    {
        $client = new \GuzzleHttp\Client();



        $response = $client->post(env('API_URL') . $url, [
            'headers' => [
                'authorization' => 'Bearer ' . session('token'),
            ],
            'multipart' => $body,
        ]);

        // $file = fopen($image_path, 'r');

        // $response = Http::withToken(session('token'))
        //     ->attach('attachment', $file)
        //     ->post(env('API_URL') . $url, $body);

        return $response;
    }
}

if (!function_exists('ApiDelCredential')) {
    function ApiDelCredential($url, $body = [])
    {
        $response = Http::withToken(session('token'))->delete(env('API_URL') . $url, $body);
        return $response;
    }
}

if (!function_exists('ApiGetWithToken')) {
    function ApiGetWithToken($url, $query = [])
    {
        $response = Http::withToken(session('token'))->get(env('API_URL') . $url, $query);
        return $response;
    }
}


if (!function_exists('setUserAuth')) {
    function setUserAuth($user, $token, $request)
    {
        session()->put('userAuth', $user);
        session()->put('token', $token);
    }
}


if (!function_exists('setUser')) {
    function setUser($user, $request)
    {
        $request->session()->put('userAuth', $user);
    }
}
