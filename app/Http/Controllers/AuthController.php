<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class AuthController extends Controller
{
    public function login()
    {
        return view('admin.pages.auth.login');
    }

    public function register()
    {
        return view('admin.pages.auth.register');
    }

    public function apiLogin(Request $request)
    {
        // dd($request);
        $response = ApiPost('/login', [
            'email' => $request->email,
            'password' => $request->password,
        ]);

        if ($response->successful()) {
            $user = $response->json("data")["user"];
            $token = $response->json("data")["token"];
            setUserAuth($user, $token, $request);
            return redirect()->route('dashboard');
        }

        return back()->withInput()->withErrors(['message' => $response->json('message')]);

        // return $response;
    }

    public function apiRegister(Request $request)
    {
        // dd($request);
        $response = ApiPost('/register', [
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password,
            'password_confirmation' => $request->password_confirmation,
        ]);
        if ($response->successful()) {
            return redirect()->route('login');
        }
        return back()->withInput()->withErrors(['message' => $response->json('message')]);
    }

    public function logout(Request $request)
    {
        $response = ApiPostCredential('/logout');
        if ($response->successful()) {
            session()->flush();
            return redirect()->route('login');
        }
        return $response;
    }
}
