<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        $response = ApiGetWithToken('/product');
        if ($response->successful()) {
            return view('admin.pages.product.index', [
                'data' => $response->json("data")
            ]);
        }
    }

    public function create()
    {
        return view('admin.pages.product.create', []);
    }

    public function edit($id)
    {
        $response = ApiGetWithToken('/product/' . $id);
        if ($response->successful()) {
            return view('admin.pages.product.edit', [
                'data' => $response->json("data")
            ]);
        }
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|min:3',
            'description' => 'nullable',
            'price' => 'required|integer',
            'stock' => 'required|integer',
            'image' => 'nullable|image|mimes:png,jpg,jpeg|max:2048',
        ]);

        try {
            $multipart = [
                [
                    'name'     => 'name',
                    'contents' => $request->name
                ],
                [
                    'name'     => 'description',
                    'contents' => $request->description
                ],
                [
                    'name'     => 'stock',
                    'contents' => $request->stock
                ],
                [
                    'name'     => 'price',
                    'contents' => $request->price
                ],
                [
                    'name'     => '_method',
                    'contents' => 'PUT'
                ],
            ];
            if ($request->hasFile('image')) {
                // AMBIL DATA KETERANGAN FILE
                $image_path = $request->image->getPathname();
                $image_mime = $request->image->getmimeType();
                $image_org  = $request->image->getClientOriginalName();
                array_push($multipart, [
                    'name'     => 'image',
                    'contents' => file_get_contents($image_path),
                    'Mime-Type' => $image_mime,
                    'filename' => $image_org,
                ]);
            }
            $response = ApiPostCredentialWithImage('/product/' . $id, $multipart);

            return redirect()->route('product.index')->with('msg', json_decode($response->getBody()->getContents())->message);
        } catch (\Throwable $th) {
            dd($th);
            return back()->withErrors($th);
        }
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|min:3',
            'description' => 'nullable',
            'price' => 'required|integer',
            'stock' => 'required|integer',
            'image' => 'nullable|image|mimes:png,jpg,jpeg|max:2048',
        ]);
        try {
            $multipart = [
                [
                    'name'     => 'name',
                    'contents' => $request->name
                ],
                [
                    'name'     => 'description',
                    'contents' => $request->description
                ],
                [
                    'name'     => 'stock',
                    'contents' => $request->stock
                ],
                [
                    'name'     => 'price',
                    'contents' => $request->price
                ],
            ];
            if ($request->hasFile('image')) {
                // AMBIL DATA KETERANGAN FILE
                $image_path = $request->image->getPathname();
                $image_mime = $request->image->getmimeType();
                $image_org  = $request->image->getClientOriginalName();
                array_push($multipart, [
                    'name'     => 'image',
                    'contents' => file_get_contents($image_path),
                    'Mime-Type' => $image_mime,
                    'filename' => $image_org,
                ]);
            }
            $response = ApiPostCredentialWithImage('/product', $multipart);
            if (json_decode($response->getBody()->getContents())->status) {
                throw new Exception(json_decode($response->getBody()->getContents())->errors);
            }
            return redirect()->route('product.index')->with('msg', json_decode($response->getBody()->getContents())->message);
        } catch (\Throwable $th) {
            dd($th);
            return back()->withErrors($th);
        }
    }

    public function delete($id)
    {
        $response = ApiDelCredential('/product/' . $id);
        if ($response->successful()) {
            return response()->json($response->json("message"));
        }
    }
}
